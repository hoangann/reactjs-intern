export interface productInterface  {
    id: string;
    description: string;
    name: string;
    price: number
}
export interface Product {
    name: string;
    price: number;
    description: string;
  }
 export interface dataProduct {
    id: string;
    name: string;
    price: string;
    description: string;
  }