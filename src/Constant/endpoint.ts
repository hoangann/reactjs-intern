const GET_LIST_PRODUCT = 'https://6684137c56e7503d1adf4096.mockapi.io/api/v1/products'

const POST_PRODUCT = 'https://6684137c56e7503d1adf4096.mockapi.io/api/v1/products'

const DELETE_PRODUCT = `https://6684137c56e7503d1adf4096.mockapi.io/api/v1/products/:id`

const EDIT_PRODUCT = `https://6684137c56e7503d1adf4096.mockapi.io/api/v1/products/:id`

export {
    GET_LIST_PRODUCT,
    POST_PRODUCT,
    DELETE_PRODUCT,
    EDIT_PRODUCT
}