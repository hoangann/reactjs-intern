import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
/*
<button type="button" className="edit" data-toggle = "modal" data-target = "editModal" data-whatever="...">Edit</button>
              <div className="modal fade" id="editModal" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title" id="editModalLabel">new message</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&time,</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form action="">
                        <div className="form-group">
                          <label htmlFor="recipient-name" className="col-form-label">Recipient:</label>
                          <input type="text" className="form-control" id="recipient-name" />
                        </div>
                        <div className="form-group">
                          <label htmlFor="message-text" className="col-form-label">Message:</label>
                          <textarea className="form-control" id="message-text"></textarea>
                        </div>
                      </form>
                    <div className="modal-footer">
                      <button type="button" className="edit " data-dismiss="modal">Close</button>
                      <button type="button" className="edit">Send message</button>
                    </div>
                    </div>
                  </div>
                  
                </div>
              </div>
*/
