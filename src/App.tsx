import { FormEvent, useEffect, useState } from "react";
import axios from "axios";
import React from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";

import { Product, productInterface } from "./Model/product";
import {
  DELETE_PRODUCT,
  GET_LIST_PRODUCT,
  POST_PRODUCT,
} from "./Constant/endpoint";


import "./App.css";

function App() {
  //TODO:
  /**
   * 1. tạo components and export + import them
   * 2. cấu trúc thư mục
   * 3. jsx là gì, tsx là gì
   * 4. hooks là gì, tại sao dùng hooks
   * 5. typescript là gì, tại sao dùng typescript
   * 6. làm việc với data
   * 7. Tính đóng gói
   * 8. Tư duy reUse
   */
  const [dataProduct, setDataProduct] = useState<productInterface[]>([]);
  const [dataEdit, setDataEdit] = useState<Product>();
  const [postAddProduct, setPostAddProduct] = useState<Product>({
    name: "aaaaa",
    price: 1 ,
    description: "aaaa",
  });
  const [isOpenModalAdd, setIsOpenModalAdd] = useState<boolean>(false);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(GET_LIST_PRODUCT);
        const result = await response.json();
        setDataProduct(result);
      } catch (error) {
        console.error("Fetch error:", error);
      }
    };
    fetchData();
  }, []);
  const handleDeleteProduct = (id: string) => {


    axios
      .delete(
        `https://6684137c56e7503d1adf4096.mockapi.io/api/v1/products/:id/${id}`
      )
      .then((res) => {
      }).catch;
  };

  const handleClick = (item: string) => {
    setDataEdit(item);
  };
  const hideElement = () => {
    const element = document?.getElementById("id01");
    if (element) {
      element.style.display = "none";
    }
  };

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPostAddProduct({
      ...postAddProduct,
      [event.target.name]: event.target.value,
    });
  };
  const handleSubmitProduct = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    axios
      .post(POST_PRODUCT, {
        postAddProduct,
      })
      .then((response) => {

        const newData = response.data.post;

        setDataProduct([newData, ...dataProduct]);
        handleToggleModalAddProduct();
      })
      .catch((err) => console.log(err));
  };

  const handleToggleModalAddProduct = () => {
    setIsOpenModalAdd(!isOpenModalAdd);
  };
  return (
    <div className="list-Product">
      <div className="button-add" onClick={handleToggleModalAddProduct}>
        Add Products
      </div>
      {dataProduct?.map((item) => {
        return (
          <div key={item?.id} className="item">
            <div className="left">
              <p>{item?.name}</p>
              <p> {item?.price}</p>
              <p> {item?.description}</p>
            </div>
            <div className="right">
              <div className="button-edit">
                <button>Edit</button>
              </div>
              <div
                key={item.id}
                className="button-delete"
                onClick={() => handleDeleteProduct(item.id)}
              >
                <button>Delete</button>
              </div>
            </div>
          </div>
        );
      })}

      <Modal
        isOpen={isOpenModalAdd}
        onRequestClose={handleToggleModalAddProduct}
        contentLabel="Example Modal"
      >
        <form id="form" onSubmit={handleSubmitProduct}>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            value={postAddProduct.name}
            onChange={handleInput}
          />
          <label htmlFor="price">Price</label>
          <input
            type="number"
            id="price"
            name="price"
            value={postAddProduct.price}
            onChange={handleInput}
          />
          <label htmlFor="description">Description</label>
          <input
            type="text"
            id="description"
            name="description"
            value={postAddProduct.description}
            onChange={handleInput}
          ></input>

          <div className="modal-footer">
            <button type="submit">Save</button>
          </div>
        </form>
      </Modal>
    </div>
  );
}

export default App;
